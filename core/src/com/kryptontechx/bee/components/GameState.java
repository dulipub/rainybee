package com.kryptontechx.bee.components;

/**
 * Created by dulip on 9/25/17.
 */
public enum GameState {
    WON, NORMAL, DEAD
}
