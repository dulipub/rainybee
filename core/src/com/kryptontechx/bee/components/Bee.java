package com.kryptontechx.bee.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.kryptontechx.bee.RainyBee;
import com.kryptontechx.bee.screens.PlayScreen;

/**
 * Created by dulip on 8/22/17.
 * bee is made fly in between y = 0 and camera height
 * it is restricted to fall any further than zero in the update method
 * and when jumping the max height is approximately kept closer to the camera height since jump won't work after
 * going beyong camera. Rely on gravity bring it back down.
 * horizontal velcity of 120 is added when creating object
 * vertical velocity of 500 is added for every jump by user
 * update method adds final physics: gravity and vertical movement by setting the position
 * since animations doesn't support the sprite, it is no longer used and instead the vector3 position is used in render method in
 * playscreen. We only update the position.
 */
public class Bee extends GameObject{

    //private Texture texInternal;
    //private Sprite sprite;
    private int gravity;
    private Circle bounds;
    private Animation<TextureRegion> animation;
    private int rows,colums;
    private float elapsedTime;
    private TextureRegion[][] tmp;

    private Texture deadImage;

    public Bee(Texture texture, int x, int y){
        //position of bee is set calling super
        super(x,y,0);
        velocity.x = 150;

        elapsedTime = 0;
        rows = 2;
        colums = 2;
        gravity=-30;
        //setting up the animation
        tmp = TextureRegion.split(texture, texture.getWidth()/colums, texture.getHeight()/rows);
        TextureRegion[] fly = new TextureRegion[rows*colums];
        int index = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colums; j++) {
                fly[index++] = tmp[i][j];
            }
        }
        animation = new Animation<TextureRegion>(0.1f,fly);
        //bounds
        bounds = new Circle();
        bounds.set(94,64,57);//(X,Y,r) really don't know why these X,Y work
        //bounds.set(76,63,50);
    }

    public void update(float dt){
        if(PlayScreen.gameState.equals(GameState.DEAD)){
            if(position.y >-200){
                velocity.add(0,-50,0); //if bee dies it drops to the ground
            }else{
                velocity.y=0;
            }
            velocity.x = 0;
        }
        if(PlayScreen.gameState.equals(GameState.WON)){
            velocity.y=0;
            velocity.x = 300;
        }
        if(PlayScreen.gameState.equals(GameState.NORMAL)){
            if(position.y < 0 ){
                //instead of directly setting the position and velocity to zero(as in previous versions scale it and set it to zero
                if(velocity.y<0){
                    velocity.y = 0;  //we allow positive velocities from y==0 but not any negative velocities
                }
            } else if( position.y> (RainyBee.WORLD_HEIGHT-120)){ //logic that doesn't allow the bee to above screen
                velocity.y = -100;
            }else if(dt>0){ //normal logic
                velocity.add(0,gravity,0);
            }
        }
        //dt should not be zero as we would get the divide by zero exception
        if(dt!=0){
            velocity.scl(dt);
            position.add(velocity.x,velocity.y,velocity.z);
            velocity.scl(1/dt);
            bounds.setPosition(position.x+40,position.y+25);
            //System.out.println("position.x  "+(position.x+32));
        }
    }

//    public void update(float dt, GameState gameState){
//
//        update(dt); //finally scale the velocities and add to positions
//    }

    public void jump(){
        float max = RainyBee.WORLD_HEIGHT -125; // height of the image
        if(position.y< max){
            velocity.y = 800;
        }
        if(velocity.x< 210){
            velocity.add(30,0,0);
        }
    }

    public void setDeadImage(Texture texture){
        deadImage = texture;
    }
    public Texture die(){
        return deadImage;
    }

    public TextureRegion getFrame(){
        elapsedTime += Gdx.graphics.getDeltaTime();
        //System.out.println(animation.getKeyFrameIndex(elapsedTime));
        return animation.getKeyFrame(elapsedTime,true);
    }

    public Circle getBounds(){
        return bounds;
    }

    @Override
    public void setSizes(float w, float h) {

    }

    @Override
    public void dispose() {

    }
}
