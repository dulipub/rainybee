package com.kryptontechx.bee.components;

import com.kryptontechx.bee.RainyBee;

import java.util.Random;

/**
 * Created by dulip on 9/11/17.
 */
public class GrainSpawn {
    private Random random;
    private Integer[] posX;
    private int spawnOrigin=0;
    private int min=50;

    public GrainSpawn(int n){
        random = new Random();
        posX = new Integer[n];
        spawnOrigin =  RainyBee.WORLD_WIDTH /(n);
        for (int i=0; i<posX.length; i++){
            posX[i] = spawnOrigin*i;
            //System.out.println(posX[i]);
        }
    }
    public int getRandom(int min, int max){
        int number = random.nextInt((max-min)+1)+min;
        return number;
    }

    public int getY(){
        int rand = getRandom(min, RainyBee.WORLD_HEIGHT-min);
        return rand;
    }

    public int getX(int id){
        posX[id]+= RainyBee.WORLD_WIDTH;
        return posX[id];
    }
}
