package com.kryptontechx.bee.components;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.utils.Array;
import com.kryptontechx.bee.screens.PlayScreen;

/**
 * Created by dulip on 8/24/17.
 * THIS CLASS MANAGES THE WATER DROPS AS THERE ARE MORE THAN ONE DROP
 *
 * texture handling - clearly passing by reference for texture creation is massively better performance
 * passing by reference:
 * new Texture is created once only in this class. Then the reference is passed as an argument to the constructor,
 * of the water class which then set it to the sprite.
 * by creation:
 * new texture is created and stored in texture variable inside the water class itself. And set to sprite internally.
 * This was so resource heavy that the game broke when tested with DROP_COUNT set to 1 million
 * for texture reference drop count = 1000000  time= (2,656043.0) micro secs and 3000=(17825.793) ms
 * for texture create drop count = 100  time= (186646.53) micro secs and 1000=(1,807745.0) and 3000=(5,364515.0)m
 *
 * update
 * takes in delta time, camera.position.x and bounds of bee
 * for every rain drop see if it collided with bee
 * if rain drop goes out of view of camera(camera.position.x - viewportWidth/2) then reposition
 * if rain drop completely falls beyond the visible height reposition,which means the entire sprite must beyond zero in y direction
 * this means the y coordinate is -water.sprite.height
 */
public class Rain {
    public static final int DROP_COUNT = 4;
    private Array<Water> rain;
    private Texture texture;
    private boolean hasCollided = false;
    private Sound pop;

    public Rain(Texture texture, Sound pop){
        //float iniTime = System.nanoTime();
        this.texture =texture;
        this.pop = pop;
        rain = new Array<Water>();
        for (int i =0; i<DROP_COUNT; i++){
            rain.add(new Water(i, this.texture));
        }
    }

    public boolean update(float dt,float cameraX, float camWidth, Circle player){

        for (Water water: rain) {
            hasCollided = water.hasCollide(player);
            float edgeWater = cameraX-water.getWidth()-camWidth/2;//slightly offset to give more time for water drop to fall
            if(hasCollided){
                PlayScreen.LIFE--;
                water.reposition(cameraX);
                pop.play();
                break;
            }else if (water.position.x < edgeWater) {
                water.reposition(cameraX);
            }else if(water.position.y < -water.getHeight()-10){ //slightly offset to give more time
                water.reposition(cameraX);
            }else {
                water.gravity(dt);
            }
        }
        return hasCollided;
    }

    public void setDifficulty(int difficulty) {
        for (Water i:rain) {
            i.setDifficulty(difficulty);
        }
    }

    public Sprite getSprite(int i){
        return rain.get(i).getSprite();
    }

    public void dispose(){
        //texture.dispose();
    }

}