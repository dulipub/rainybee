package com.kryptontechx.bee.components;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by dulip on 9/9/17.
 */
public abstract class GameObject {

    protected Vector3 position;
    protected Vector3 velocity;

    public GameObject(){
        position = new Vector3(0,0,0);
        velocity = new Vector3(0,0,0);
    }

    public GameObject(int x, int y, int z){
        position = new Vector3(x,y,z);
        velocity = new Vector3(0,0,0);
    }

    public abstract void setSizes(float w, float h);
    public abstract void update(float dt);
    public abstract void dispose();

    public Vector3 getPosition() {
        return position;
    }
    public float getPositionX() {
        return position.x;
    }
    public float getPositionY() {
        return position.y;
    }
    public Vector3 getVelocity(){return  velocity;}
}
