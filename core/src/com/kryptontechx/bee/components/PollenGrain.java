package com.kryptontechx.bee.components;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.kryptontechx.bee.screens.PlayScreen;

/**
 * Created by dulip on 9/11/17.
 *
 * pollen should disappear and reposition once player collects them or go out of the view of the camera
 * they should appear at constant x distances with random heights(ie relative position.x is constant to a grain but y is random)
 * therefore no need to randomize spawning in the x direction just add the WORLD_WIDTH to current x position
 */
public class PollenGrain extends GameObject {

    private Sprite sprite;
    private Circle bounds;
    private GrainSpawn spawn;
    private int id;
    private Sound reward;

    public PollenGrain(int id, Texture texture, GrainSpawn spawn, Sound reward){
       // System.out.print("pollen created");
        this.id = id;
        sprite = new Sprite(texture);
        bounds = new Circle();
        this.spawn = spawn;
        this.reward = reward;

        setSizes(100,100);
        reposition(id);
    }
    @Override
    public void setSizes(float w, float h) {
        //sprite.setAlpha(0.9f);
        sprite.setSize(w,h);
        bounds.set(25,25,25);
    }

    @Override
    public void update(float dt) {

    }

    public void reposition(float cameraX){
        velocity.set(0,0,0);
        position.x = spawn.getX(id);
        position.y = spawn.getY();
        sprite.setPosition(position.x, position.y);
        bounds.setPosition(position.x,position.y);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void hasCollided(float cameraX,Circle player){
        boolean picked = player.overlaps(bounds);
        if(picked){
            PlayScreen.POLLEN +=1;
            reward.play(0.3f);
            //System.out.println("picked "+PlayScreen.POLLEN);
            reposition(cameraX);
        }
    }

    @Override
    public void dispose() {

    }
}
