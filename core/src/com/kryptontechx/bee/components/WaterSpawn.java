package com.kryptontechx.bee.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.kryptontechx.bee.RainyBee;

import java.util.Random;

/**
 * Created by dulip on 9/9/17.
 */
public class WaterSpawn {
    private Random random;
    private Integer[] posY = {0,2,5,-5,2,8,-2,0};
    private Integer[] posX;
    private int spawnOrigin=0;

    //for rain
    public WaterSpawn(){
        random = new Random();
        //positions of x we want to capture:
        //0,320,640,960 and 1280
        posX = new Integer[5];
        //this will give 320
        spawnOrigin =  RainyBee.WORLD_WIDTH /4;

        //i will be starting from zero to five
        for (int i=0; i<posX.length; i++){
            posX[i] = spawnOrigin*i;
            //System.out.println(posX[i]);
        }
    }
    public int getRandom(int min, int max){
        int number = random.nextInt((max-min)+1)+min;
        return number;
    }

//    public int getY(){
//        int rand = getRandom(0,RainyBee.WORLD_HEIGHT);
//        return rand;
//    }

    public int getY(float height){
        int h = (int)height+1;
        int rand = 150+getRandom(h,h*6);//where 150 is the height of 1.5*bee
        return rand;
    }

    public int getX(int id){
        int mod = id%5;
        int rand = getRandom(-spawnOrigin,spawnOrigin);
        return posX[mod]+rand;
    }
}