package com.kryptontechx.bee.components;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.kryptontechx.bee.RainyBee;

/**
 * Created by dulip on 9/11/17.
 * THIS CLASS MANAGES THE POLLEN GRAINS AS THERE ARE MORE THAN ONE GRAIN
 */
public class Pollen{
    public static int POLLENCOUNT = 4;
    private Array<PollenGrain> grains;
    private GrainSpawn spawn;

    public Pollen(Texture texture, Sound reward){

        spawn = new GrainSpawn(POLLENCOUNT);
        grains = new Array<PollenGrain>();
        for(int i=0; i<POLLENCOUNT; i++){
            grains.add(new PollenGrain(i,texture,this.spawn,reward));
        }
    }

    public void update(float dt,float cameraX, float camWidth, Circle player){
        for (PollenGrain grain:grains) {
            if(grain.position.x<cameraX-camWidth){
                grain.reposition(cameraX);
            }else{
                grain.hasCollided(cameraX,player);
            }
        }
    }

    public Sprite getSprite(int i){
        return grains.get(i).getSprite();
    }

    public void dispose() {

    }
}
