package com.kryptontechx.bee.components;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

import java.util.Stack;

/**
 * Created by dulip on 12/10/17.
 */
public class ButtonStack {
    private Stack<Button> buttons;

    public ButtonStack(){
        buttons = new Stack<Button>();
    }

    public void push(Button button){
        buttons.push(button);
    }

    public void pop(){
        buttons.pop();
    }

    public void set(Button button){
        buttons.pop();
        buttons.push(button);
    }

//    public void update(float dt){
//        buttons.peek().update(dt);
//    }

    public void add(Stage stage){
        stage.addActor(buttons.peek());
    }
}
