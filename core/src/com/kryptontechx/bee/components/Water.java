package com.kryptontechx.bee.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.kryptontechx.bee.RainyBee;

/**
 * Created by dulip on 8/24/17.
 * sprite is set to 75,135 world units
 * bounds are slightly less than that to incorperate the tail of the water drop
 * spawning is handled in the spawn manager
 * collision is handled by bounds
 *
 * gravity:
 * gravity and velocity is negative as it falls down
 * gravitational acceleration constant is set and added every update if the velocity of water does not exceed -400 units,
 * if equal to limit then it is kept constant(like viscous forces balancing out gravity)
 * since they start at a large height they are not allowed to go too fast
 * scaling is done only if game is not paused/minimized in device therefore resulting in dt==0
 * to avoid dividing by 1/0
 *
 * reposition:
 * velocity is set to zero since falling again from the top.
 * corresponding x value is got and the camera position is added so that water drops will be at least visible
 * and not beyond the view of the camera viewport.
 * It is important to have water drops all over the screen therefore the midpoint is choosen as the origin and a value
 * positive or negative is added(water drops are generated inside "lanes" in spawn manager)
 * positon of sprite and bounds must be the same
 *
 * update:
 * is not used here as rain.update() is used. This is a special case.
 * rain.update() calls gravity and reposition
 */
public class Water extends GameObject {
    private Sprite sprite;
    private float height,width;
    private int id;
    private int gravity= -10;
    private WaterSpawn spawn;
    private Circle bounds;
    private float difficulty=320.0f;

    public Water(int id, Texture texture){
        this.id = id;
        this.sprite = new Sprite(texture);
        spawn = new WaterSpawn();
        bounds = new Circle();
        //setting the sizes of various objects
        setSizes(100,100);
        //random initial positions
        reposition(id);
    }

    @Override
    public void setSizes(float w, float h) {
        height=h;
        width=w;
        this.sprite.setSize(w,h);
        this.sprite.setAlpha(0.75f);
        this.bounds.set(50,50,48);
    }

    protected void gravity(float dt) {
        if(velocity.y > -difficulty){
            velocity.add(-2,gravity,0);
        }
        if(dt>0){
            velocity.scl(dt);
            position.add(velocity.x,velocity.y,velocity.z);
            velocity.scl(1/dt);
        }
        sprite.setPosition(position.x,position.y);
        bounds.setPosition(position.x,position.y); //to correctly neglect the tail of the water drop
    }

    protected void reposition(float cameraX){
        velocity.set(0,0,0);
        position.x = cameraX+ spawn.getX(this.id);
        position.y = RainyBee.WORLD_HEIGHT +spawn.getY(height);
        sprite.setPosition(position.x, position.y);
        bounds.setPosition(position.x,position.y);//to correct the reduce height of the water drop
    }

    public void update(float dt){ return;}

    public boolean hasCollide(Circle player){
        return player.overlaps(bounds);
    }

    public void setDifficulty(float difficulty) {
        this.difficulty = difficulty;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public float getHeight() {
        return height;
    }

    public float getWidth() {
        return width;
    }

    @Override
    public void dispose() {
        this.sprite.getTexture().dispose();
    }

}