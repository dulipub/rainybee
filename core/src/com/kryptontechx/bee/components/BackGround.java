package com.kryptontechx.bee.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.kryptontechx.bee.RainyBee;

/**
 * Created by dulip on 8/23/17.
 * two sprites overlapping each other
 * bgs - bach ground sprite
 * leftEdge is the cameras edge of vision
 * bgsEdge is the right edges of the respective backgrounds
 * which is equal to the position from bottom left + sprite width
 * update logic:
 * when the right edge passes the cameras last view it will reposition itself to the right edge of the other sprite
 * therefore set position is done with other edge position variable
 *
 * this is terrible code
 * bgs1 = new Sprite(background);
 * bgs2 = new Sprite(bgs1);
 * as we are creating two sprite objects( new Keyword is used twice) which have the heaviest texture
 * instead use one texture and two vectors for position
 * Also we cannot do something like this
 * bgs1 = new Sprite(background);
 * bgs2 = bgs1;
 * as then both references point to same object with only one set of coordinates for position
 */
public class BackGround{
    private Texture background;
    public Vector2 positions1, positions2;
    private Sprite bgs1,bgs2;

    public BackGround(Texture background){
        bgs1 = new Sprite(background);
        bgs2 = new Sprite(bgs1);
        setSizes(RainyBee.WORLD_WIDTH,RainyBee.WORLD_HEIGHT);
    }
    private void setSizes(float w, float h) {
        bgs1.setSize(w,h);
        bgs2.setSize(w,h);
        bgs1.setPosition(0,0);
        bgs2.setPosition(bgs1.getWidth(),0);
    }

    public void update(float dt, float cameraX, float camWidth){
        float leftEdge = cameraX- RainyBee.WORLD_WIDTH/2;
        float bgsEdge1 = bgs1.getX()+bgs1.getWidth();
        float bgsEdge2 = bgs2.getX()+bgs2.getWidth();
        if(bgsEdge1< leftEdge){
            bgs1.setPosition(bgsEdge2-1,0);
        }else if(bgsEdge2<leftEdge){
            bgs2.setPosition(bgsEdge1-1,0);
        }
    }

    public Sprite getBgs1() {
        return bgs1;
    }

    public Sprite getBgs2() {
        return bgs2;
    }
    public void dispose(){
       // bgs1.getTexture().dispose();
    }
}

/*
* public class BackGround{
    private Texture background;
    public Vector2 positions1, positions2;

    public BackGround(Texture background){
        this.background = background;
        positions1 = new Vector2(0,0);
        positions2 = new Vector2(background.getWidth(),0);
        System.out.println(this.background);
    }

    public void update(float dt, float cameraX, float camWidth){
        float leftEdge = cameraX- RainyBee.WORLD_WIDTH/2;
        float bgsEdge1 = positions1.x+background.getWidth();
        float bgsEdge2 = positions2.x+background.getWidth();

        if(bgsEdge1< leftEdge){
            positions1.x = bgsEdge2;
        }
        else if(bgsEdge2<leftEdge){
            positions2.x = bgsEdge2;
        }
    }

    public Texture getBackground(){
        return background;
    }

    public void dispose(){
       // bgs1.getTexture().dispose();
    }
}
* */