package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kryptontechx.bee.RainyBee;
import com.kryptontechx.bee.components.GameState;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;

/**
 * Created by dulip on 9/16/17.
 */
public class EndScreen implements Screen {
    private RainyBee app;
    private Stage stage;
    private Image bg;
    private Skin skin;
    private TextButton btnMenue, btnAd;
    private ImageButton btnRestart;
    private Image star1, star2, star3;
    private Sound click;
    private int finalScore , timesTried, timesWon;
    private Image board;
    private Label lblTop, lblScore, lblAd,lblNone,lblWon,lblAttempts;
    //private Preferences prefs;

    public EndScreen(RainyBee app){
        this.app = app;
        stage = new Stage(new StretchViewport(RainyBee.MWIDTH,RainyBee.MHEIGHT,app.camera)); //(630,630)
        skin = new Skin();
    }
    @Override
    public void show() {
        stage.clear();
        app.camera.position.x = RainyBee.MWIDTH/2;
        app.camera.position.x = RainyBee.MHEIGHT/2;
        //bgTexture = app.assets.get("png/bgHills.png", Texture.class);
        bg = new Image(app.assets.get("png/bg.png", Texture.class));
        bg.setSize(RainyBee.WORLD_WIDTH,RainyBee.WORLD_HEIGHT);
        bg.setPosition(0,0);
        stage.addActor(bg);

        board = new Image(app.assets.get("png/end.png", Texture.class));
        board.setPosition(50,50);
        stage.addActor(board);

        skin.addRegions(app.assets.get("ui/orange/uiskin.atlas", TextureAtlas.class));
        skin.add("default-font",app.font24);
        skin.load(Gdx.files.internal("ui/orange/uiskin.json"));
        Gdx.input.setInputProcessor(stage);
        initButtons();

        click = app.assets.get("sound/button.ogg", Sound.class);

        int nStars = calScore(); //calculate score and return the number of starts need to be draw(0,1,2 or 3)

        timesTried = app.preferences.getInteger("attempts");
        timesWon = app.preferences.getInteger("won");
        timesTried++;
        app.preferences.putInteger("attempts",timesTried);
        if(PlayScreen.gameState.equals(GameState.WON)){
            timesWon++;
            app.preferences.putInteger("won",timesWon); // counts the times which player won
            initDecos(nStars,true);
        }else{
            initDecos(nStars, false);
        }
        app.preferences.flush();
    }

    private void initButtons() {
        btnRestart = new ImageButton(skin);
        btnAd = new TextButton("watch ad", skin,"default");
        btnMenue =  new TextButton("MENUE", skin, "default");

        TextureRegionDrawable replay = new TextureRegionDrawable(new TextureRegion(new Texture("ui/replay.png")));

        btnRestart.setSize(110,90);
        btnRestart.setPosition(65,90);
        btnRestart.getStyle().imageUp = replay;
        btnRestart.getStyle().imageDown = replay;
        btnRestart.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.playScreen);
            }
        });
        btnRestart.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));

        btnAd.setSize(110,90);
        btnAd.setPosition(260,90); //same y but different x this time
        btnAd.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Dialog dialog = new Dialog("Info",skin);
                dialog.text("Turn on Wifi or Data");
                dialog.button("OK", true);
                dialog.show(stage);
            }
        });
        btnAd.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));

        btnMenue.setSize(110,90);
        btnMenue.setPosition(455,90);
        btnMenue.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.mainMenue);
            }
        });
        btnMenue.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));

        stage.addActor(btnRestart);
        stage.addActor(btnAd);
        stage.addActor(btnMenue);
    }

    private int calScore() {
        finalScore = (PlayScreen.POLLEN*10)+(300-PlayScreen.TIME);
//        int pollen = PlayScreen.POLLEN+app.preferences.getInteger("pollen");
//        app.preferences.putInteger("pollen",pollen);
//        app.preferences.flush();
//        System.out.println("pollen "+pollen);
        //System.out.println(finalScore);
        if(finalScore<200 && finalScore>=100){
            return 1;
        }else  if(finalScore<300 && finalScore>=200){
            return 2;
        }else if(finalScore>=300){
            return 3;
        }else
            return 0;
    }

    private void initDecos(int nStarts, boolean hasWon){

        String lablelText = "";
        if(hasWon){
            lablelText = "Congratz You Won!";
        }else{
            lablelText = "Game Over";
        }
        float center = RainyBee.MWIDTH/2 ;

        lblTop = new Label(lablelText, new Label.LabelStyle(app.font48,Color.GOLD));
        lblTop.setPosition( center - lblTop.getWidth()/2,520);

        lblScore = new Label(String.format("Score: %d",finalScore), new Label.LabelStyle(app.font48,Color.GOLD));
        lblScore.setPosition( center - lblScore.getWidth()/2,450);

        lblNone = new Label("Sorry No Stars", new Label.LabelStyle(app.font48,Color.GOLD));
        lblNone.setPosition(center-lblNone.getWidth()/2,340 );
        lblNone.addAction(alpha(0));

        lblWon = new Label(String.format("Won: %d",timesWon), new Label.LabelStyle(app.font24G,Color.GOLD));
        lblWon.setPosition(center-140,220);

        lblAttempts = new Label(String.format("Attempts: %d",timesTried), new Label.LabelStyle(app.font24G,Color.GOLD));
        lblAttempts.setPosition(center+40,220);

        lblAd = new Label("watch ad to get an extra life", new Label.LabelStyle(app.font24G,Color.GOLD));
        lblAd.setPosition(center-lblAd.getWidth()/2,190);

        star1= new Image(app.assets.get("ui/star3.png", Texture.class));
        star1.setSize(120,120);
        star1.addAction(alpha(0));

        star2= new Image(app.assets.get("ui/star3.png", Texture.class));
        star2.setSize(120,120);
        star2.addAction(alpha(0));

        star3= new Image(app.assets.get("ui/star3.png", Texture.class));
        star3.setSize(120,120);
        star3.addAction(alpha(0));

        //this is the adjustment for the star position x
        float starAdj = star1.getWidth()/2;
        //we have to reposition the starts if only one or two of them are displayed
        if(nStarts ==0){
           lblNone.addAction(fadeIn(1));
        }else if(nStarts ==1){
            star1.setPosition(center-starAdj,300);
            star1.addAction(sequence(
                    fadeIn(2, Interpolation.exp5)
            ));
        }else if(nStarts ==2){
            star1.setPosition(center-starAdj*2,300);
            star2.setPosition(center,260);
            star1.addAction(sequence(
                    fadeIn(2, Interpolation.exp5)
            ));
            star2.addAction(sequence(
                    fadeIn(2,Interpolation.exp5)
            ));
        }else if(nStarts ==3){
            star1.setPosition(center-starAdj*3,300);
            star2.setPosition(center-starAdj,300);
            star3.setPosition(center+starAdj,300);
            star1.addAction(sequence(
                    fadeIn(2, Interpolation.exp5)
            ));
            star2.addAction(sequence(
                    fadeIn(2,Interpolation.exp5)
            ));
            star3.addAction(sequence(
                    fadeIn(2,Interpolation.exp5)
            ));
        }

        stage.addActor(lblTop);
        stage.addActor(lblScore);
        stage.addActor(lblAd);
        stage.addActor(lblNone);
        stage.addActor(lblAttempts);
        stage.addActor(lblWon);
        stage.addActor(star1);
        stage.addActor(star2);
        stage.addActor(star3);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        update(delta);
        stage.draw();
    }
    private void update(float delta){
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
