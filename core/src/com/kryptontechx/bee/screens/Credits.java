package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kryptontechx.bee.RainyBee;

/**
 * Created by dulip on 10/7/17.
 */
public class Credits implements Screen {
    private RainyBee app;
    private String details="";
    private TextButton btnBack;
    private Skin skin;
    private Stage stage;
    private Sound click;

    public Credits(RainyBee app){
        this.app = app;
        stage = new Stage(new StretchViewport(RainyBee.WORLD_WIDTH/2,RainyBee.WORLD_HEIGHT,app.camera),app.batch);

        FileHandle handle = Gdx.files.internal("about.txt");
        details = handle.readString();
        skin = new Skin();
    }
    @Override
    public void show() {
        stage.clear();
        skin.addRegions(app.assets.get("ui/orange/uiskin.atlas", TextureAtlas.class));
        skin.add("default-font", app.font36);
        skin.load(Gdx.files.internal("ui/orange/uiskin.json"));
        Gdx.input.setInputProcessor(stage);

        click = app.assets.get("sound/button.ogg", Sound.class);
        initScreen();
    }

    private void initScreen() {
        btnBack = new TextButton("Back",skin);
        btnBack.setSize(100,80);
        btnBack.setPosition(RainyBee.WORLD_WIDTH/2-120,RainyBee.WORLD_HEIGHT-100);
        btnBack.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.mainMenue);
            }
        });
        stage.addActor(btnBack);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);

        app.batch.begin();
        app.font24.draw(app.batch,details,50,500);
        app.batch.end();
        stage.draw();
    }


    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }
}
