package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kryptontechx.bee.RainyBee;

/**
 * Created by dulip on 10/7/17.
 */
public class Settings implements Screen {
    private RainyBee app;
    private Stage stage;
    private Skin skin;
   // private Preferences prefs;
    private Sound click;
    private TextButton btnBack;
    private CheckBox btnYellow,btnGreen, btnNinja,btnQueen;
    private ButtonGroup<CheckBox> chbGroup;
    private Slider adjMusic;
    private Label lblMusic,lblBee, lblDif;
    private SelectBox<String> selectBox;

    private int musicVal=40;

    public Settings(RainyBee app){
        this.app = app;
        stage = new Stage(new StretchViewport(RainyBee.WORLD_WIDTH/2,RainyBee.WORLD_HEIGHT,app.camera),app.batch);
        skin = new Skin();
    }

    @Override
    public void show() {
        stage.clear();
        skin.addRegions(app.assets.get("ui/orange/uiskin.atlas", TextureAtlas.class));
        skin.add("default-font", app.font36);
        skin.load(Gdx.files.internal("ui/orange/uiskin.json"));
        Gdx.input.setInputProcessor(stage);

        click = app.assets.get("sound/button.ogg", Sound.class);
        setLogic();  //logic for screen
        initScreen(); //GUI for screen
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1f, 1f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    private void setLogic() {
        //boolean hasWon = app.preferences.getBoolean("hasWon");
        musicVal = app.preferences.getInteger("music");
    }

    private void initScreen() {
        lblMusic = new Label("Music",  new Label.LabelStyle(app.font48, Color.GOLD));
        lblMusic.setPosition(50,RainyBee.WORLD_HEIGHT-200);

        lblBee = new Label("Select Bee", new Label.LabelStyle(app.font48,Color.GOLD));
        lblBee.setPosition(50,RainyBee.WORLD_HEIGHT-275);

        lblDif = new Label("Difficulty", new Label.LabelStyle(app.font48,Color.GOLD));
        lblDif.setPosition(50,RainyBee.WORLD_HEIGHT-400);

        adjMusic = new Slider(0,100,10,false,skin);
        adjMusic.setSize(200,10);
        adjMusic.setPosition(300,RainyBee.WORLD_HEIGHT-190);
        adjMusic.setValue(musicVal);
        adjMusic.addListener(new ChangeListener(){
            //because slider.getvalue gives value between 1-100 we have to multiply this by 0.01 to set the music correctly
                public void changed (ChangeEvent event, Actor actor){
                    app.music.setVolume(adjMusic.getValue()*0.01f);
                    app.preferences.putInteger("music",(int)adjMusic.getValue());
                }
        });

        selectBox = new SelectBox<String>(skin,"rainybee");
        selectBox.setItems("easy","normal","hard");
        selectBox.setSize(200,50);
        selectBox.setPosition(300,RainyBee.WORLD_HEIGHT-400);
        int setIndex = app.preferences.getInteger("difficulty");
        if(setIndex==400){
            selectBox.setSelectedIndex(2);
        }else if(setIndex==375){
            selectBox.setSelectedIndex(1);
        }else {
            selectBox.setSelectedIndex(0);
        }
        selectBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                //default difficulty is zero
                int index = selectBox.getSelectedIndex();
                if(index==2){
                    app.preferences.putInteger("difficulty",400);
                }else if(index==1){
                    app.preferences.putInteger("difficulty",375);
                }else{
                    app.preferences.putInteger("difficulty",350);
                }
                app.preferences.flush();
            }
        });

        btnBack = new TextButton("Back",skin);
        btnBack.setSize(100,80);
        btnBack.setPosition(RainyBee.WORLD_WIDTH/2-120,RainyBee.WORLD_HEIGHT-100);
        btnBack.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.mainMenue);
                //when we exit the settings we flush the prefernaces so that only final values get saved
                //instead of calling flush over and over again for every change
                app.preferences.flush();
            }
        });

        btnYellow = new CheckBox("Yellow",skin,"radio");
        btnYellow.setSize(100,100);
        btnYellow.setPosition(50,RainyBee.WORLD_HEIGHT-350);
        btnYellow.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                app.preferences.putInteger("beeType",0); //this is used in play screen
            }
        });

        btnGreen = new CheckBox("Green",skin,"radio");
        btnGreen.setSize(100,100);
        btnGreen.setPosition(175,RainyBee.WORLD_HEIGHT-350);
        btnGreen.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                app.preferences.putInteger("beeType",1);
            }
        });

        btnNinja = new CheckBox("Dark",skin,"radio");
        btnNinja.setSize(100,100);
        btnNinja.setPosition(300,RainyBee.WORLD_HEIGHT-350);

        btnQueen = new CheckBox("Queen",skin,"radio");
        btnQueen.setSize(100,100);
        btnQueen.setPosition(425,RainyBee.WORLD_HEIGHT-350);

        chbGroup = new ButtonGroup<CheckBox>(btnYellow,btnGreen, btnNinja,btnQueen);
        chbGroup.setMaxCheckCount(1);
        chbGroup.setMinCheckCount(1);
        chbGroup.setUncheckLast(true);

        stage.addActor(lblMusic);
        stage.addActor(lblBee);
        stage.addActor(adjMusic);
        stage.addActor(selectBox);
        stage.addActor(lblDif);

        stage.addActor(btnBack);
        stage.addActor(btnYellow);
        stage.addActor(btnGreen);
        stage.addActor(btnNinja);
        stage.addActor(btnQueen);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
