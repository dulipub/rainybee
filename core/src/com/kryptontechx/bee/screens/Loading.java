package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Timer;
import com.kryptontechx.bee.RainyBee;

/**
 * Created by dulip on 9/4/17.
 */
public class Loading implements Screen {
    private RainyBee app;
    private BitmapFont beeFacts;
    private BitmapFont loadText;
    private boolean timerIsOn;

    public Loading(RainyBee app){
        this.app = app;
        beeFacts = new BitmapFont();
        loadText = new BitmapFont();
    }

    @Override
    public void show() {
        timerIsOn = false;
        quenueAssets();
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        app.batch.begin();
        loadText.draw(app.batch,"LOADING....",(RainyBee.WORLD_WIDTH*0.2f),RainyBee.WORLD_HEIGHT/2+50);
        beeFacts.draw(app.batch,"It's about to rain and your bee needs to get home",(RainyBee.WORLD_WIDTH*0.05f),RainyBee.WORLD_HEIGHT/2);
        beeFacts.draw(app.batch,"Survive for five minutes, take him to his hive safely",(RainyBee.WORLD_WIDTH*0.05f),RainyBee.WORLD_HEIGHT/2-30);
        app.batch.end();
    }

    private void update(float delta) {

        if(!timerIsOn && app.assets.update()) {
            timerIsOn = true;
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    app.setScreen(app.mainMenue);
                }

            }, 1);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        beeFacts.dispose();
        loadText.dispose();
    }

    private void quenueAssets(){
        app.assets.load("png/bg.png", Texture.class);
        app.assets.load("png/end.png", Texture.class);
        app.assets.load("png/water_circle.png", Texture.class);
        app.assets.load("png/grain7.png",Texture.class);
        app.assets.load("ui/star3.png", Texture.class);
        app.assets.load("png/BeeHD.png", Texture.class);
        app.assets.load("png/BeeNew1.png",Texture.class);
        app.assets.load("png/BeeAll4.png",Texture.class);
        app.assets.load("png/grainHD.png",Texture.class);
        app.assets.load("png/cover.png",Texture.class);
        app.assets.load("png/menue2.png",Texture.class);
        app.assets.load("ui/play.png", Texture.class);

        app.assets.load("ui/orange/uiskin.atlas", TextureAtlas.class);
        app.assets.load("ui/menue_ui.atlas", TextureAtlas.class);

        app.assets.load("sound/reward.mp3", Sound.class);
        app.assets.load("sound/pop.ogg", Sound.class);
        app.assets.load("sound/button.ogg", Sound.class);
    }

}
