package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kryptontechx.bee.RainyBee;

/**
 * Created by dulip on 11/26/17.
 */
public class Unlocks implements Screen {
    private RainyBee app;
    private Stage stage;
    private Skin skin;
    private Image backGround;
    private TextButton btnBack, btnRB,btnYB;
    private ImageButton imb1,imb2,imb3;


    public Unlocks(RainyBee app){
        this.app = app;
        stage = new Stage(new StretchViewport(RainyBee.WORLD_WIDTH/2,RainyBee.WORLD_HEIGHT,app.camera),app.batch);
        skin = new Skin();
    }

    @Override
    public void show() {
        stage.clear();
        app.camera.position.x = RainyBee.WORLD_WIDTH/4; // since our camera moves in the playscreen we have to reset it here, as we are not creating new menue
        //app.music.setVolume(0.6f);
        skin.addRegions(app.assets.get("ui/orange/uiskin.atlas", TextureAtlas.class));
        skin.add("default-font",app.font36); //changed the orange/uiskin.json to this
        skin.load(Gdx.files.internal("ui/orange/uiskin.json"));
        Gdx.input.setInputProcessor(stage);
        initButtons();

    }

    private void initButtons() {
        backGround = new Image(app.assets.get("png/cover.png",Texture.class));
        backGround.setPosition(0,0);

        int ho = 74;
        TextureAtlas atlas = app.assets.get("ui/menue_ui.atlas",TextureAtlas.class);
        SpriteDrawable img1 = new SpriteDrawable(new Sprite(atlas.findRegion("menue_large")));
        SpriteDrawable img2 = new SpriteDrawable(new Sprite(atlas.findRegion("replay")));
        SpriteDrawable img3 = new SpriteDrawable(new Sprite(atlas.findRegion("settings")));

        imb1 = new ImageButton(skin,"imb1");
        imb1.setSize(144,144);
        imb1.setPosition(RainyBee.WORLD_WIDTH/4-ho,100);
        imb1.getStyle().imageDown = img1;
        imb1.getStyle().imageUp = img1;

        imb2 = new ImageButton(skin,"imb2");
        imb2.setSize(144,144);
        imb2.setPosition(RainyBee.WORLD_WIDTH/4-ho*4,100);
        imb2.getStyle().imageDown = img2;
        imb2.getStyle().imageUp = img2;


        imb3 = new ImageButton(skin,"imb3");
        imb3.setSize(144,144);
        imb3.setPosition(RainyBee.WORLD_WIDTH/4+ho*2,100);
        imb3.getStyle().imageDown = img3;
        imb3.getStyle().imageUp = img3;

        btnBack = new TextButton("Back",skin);
        btnBack.setSize(100,80);
        btnBack.setPosition(RainyBee.WORLD_WIDTH/2-120,RainyBee.WORLD_HEIGHT-100);
        btnBack.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                app.setScreen(app.mainMenue);
            }
        });

        btnRB = new TextButton("Back",skin);
        btnRB.setSize(100,80);
        btnRB.setPosition(RainyBee.WORLD_WIDTH/2-120,RainyBee.WORLD_HEIGHT-240);
        btnRB.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                app.setScreen(app.mainMenue);
            }
        });

        btnYB = new TextButton("Back",skin);
        btnYB.setSize(100,80);
        btnYB.setPosition(RainyBee.WORLD_WIDTH/2-120,RainyBee.WORLD_HEIGHT-380);
        btnYB.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                app.setScreen(app.mainMenue);
            }
        });

        stage.addActor(backGround);
        stage.addActor(btnBack);
        stage.addActor(imb1);
        stage.addActor(imb2);
        stage.addActor(imb3);
    }

    @Override
    public void render(float delta) {
        stage.act();
        Gdx.gl.glClearColor(1.0f,1.0f, 1.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
