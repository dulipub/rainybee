package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.viewport.*;
import com.kryptontechx.bee.RainyBee;
import com.kryptontechx.bee.components.*;

import java.util.HashMap;

/**
 * Created by dulip on 8/21/17.
 *
 * Stetch viewport is used to reduce the hassle, even though it is not ideal. Otherwise the water drop can be seen spawning if
 * view height exceed(in extend-viewport), other ports are not good as well as we reposiotn stuff they can be seen if
 * height or width exceed the world h or w.
 *
 * Extends state class where all the camera and viewports(WORLD_W a nd WORLD_H variables) are set
 * camera is set to follow the bee with an offset
 * cameraX is the approximate location of the camera x axis, to be accessed in components
 * this is the backbone of the game where all the gameplay is created and handled
 * keep this class simple as possible, do all the updates of components in there update method
 *
 * update:
 * the bounds of the bee is passed on to the update of the rain to see if the bee has lost with any of the rain drops
 *
 *  bee and background is continuosly drawn and updated. Since we want the bee to drop down when dead.
 *  The rain and pollen is not displayed when bee is dead
 *
 *  gamestate is changed and looked in the endscreen to see if it is won
 */
public class PlayScreen implements Screen {

    public static int POLLEN;
    public static int TIME;
    public static int LIFE;
    public static GameState gameState;

    private RainyBee app;
    private Viewport viewport;
    private Pollen pollen;
    private BackGround background;
    private Bee bee;
    private Rain rain;
    private TextureRegion currentFrame;

    private float textOffset;
    private int period;
    private float timeSeconds;
    private HashMap<Integer,String> beeType;
    int setBee = 0;

    private String score;
    private String time;
    private BitmapFont gameover;

/* initial all the game components
 *  static variables must be set to zero eact time play screen is set as screen in app
 *  this also apply to all other moving variables that are not created when setScreen(app.playscreen) is called
 *  for example app.camera has moved therefore need toreposition
 *  But we create new gamobjects to simplify things and reuse only the textures through the asset manager
 *  since the camera follows the bee you cannot set bee at (0,350) since left half of camera will go out of bounds
 *  initially the bee is set at position half of view minus offset
 *  sprite batch set projection matrix tells the sprite batch which camera to use with it.
 *
 *  time is to survive 5 mins(300s) time will be counted down instead of counting up to  make "stakes higher"
 */
    public PlayScreen(RainyBee app){
        this.app = app;
        viewport = new StretchViewport(RainyBee.WORLD_WIDTH/2,RainyBee.WORLD_HEIGHT,this.app.camera);
        viewport.apply();

        gameover = new BitmapFont();
        gameover.setColor(Color.BLACK);
        textOffset = RainyBee.WORLD_WIDTH/4;
        period = 1;
        score = "Pollen : ";
        time = "Time: ";

        //contains the path of the different bee images
        beeType = new HashMap<Integer, String>();
        beeType.put(0,"png/BeeHD.png");
        beeType.put(1,"png/BeeHD.png");
        beeType.put(2,"png/BeeHD.png");
        beeType.put(3,"png/BeeHD.png");
    }

    @Override
    public void show() {
        POLLEN =0;
        LIFE = 2;
        TIME = 300;
        gameState = GameState.NORMAL;
        app.camera.position.set(RainyBee.WORLD_WIDTH/2,RainyBee.WORLD_HEIGHT/2,0);
        app.camera.update();
        //app.music.setVolume(0.3f);

        background = new BackGround(app.assets.get("png/bg.png",Texture.class));
        rain = new Rain(app.assets.get("png/water_circle.png", Texture.class),app.assets.get("sound/pop.ogg", Sound.class));
        pollen = new Pollen(app.assets.get("png/grainHD.png",Texture.class),app.assets.get("sound/reward.mp3", Sound.class));
        //using hashmap to get file path
        setBee = app.preferences.getInteger("beeType");
        bee = new Bee(app.assets.get(beeType.get(setBee), Texture.class), RainyBee.WORLD_WIDTH/2-300,350);
        bee.setDeadImage(app.assets.get("png/BeeNew1.png",Texture.class));
        setDifficulty();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        app.batch.setProjectionMatrix(app.camera.combined);
        update(delta);

        app.batch.begin();
        background.getBgs1().draw(app.batch);
        background.getBgs2().draw(app.batch);
//        app.batch.draw(background.getBackground(),background.positions1.x,background.positions1.y);
//        app.batch.draw(background.getBackground(),background.positions2.x,background.positions2.y);

        //probably not good if you use a switch statement here since we cannot put boolean(&&,||) logic
        // in the case statements while with if else we can
        if(gameState.equals(GameState.DEAD)){
            app.batch.draw(bee.die(),bee.getPositionX(),bee.getPositionY());
            gameover.draw(app.batch,"Game Over",app.camera.position.x-20,app.camera.position.y);
        }else if(gameState.equals(GameState.WON)){
            currentFrame = bee.getFrame();
            app.batch.draw(currentFrame,bee.getPositionX(),bee.getPositionY());
            gameover.draw(app.batch,"WON",app.camera.position.x-20,app.camera.position.y);
        }else{
            currentFrame = bee.getFrame();
            //System.out.println(currentFrame.getRegionWidth()+" , "+currentFrame.getRegionHeight());
            app.batch.draw(currentFrame,bee.getPositionX(),bee.getPositionY());
            for(int i=0; i<Pollen.POLLENCOUNT; i++){
                pollen.getSprite(i).draw(app.batch);
            }
            for(int i=0; i<Rain.DROP_COUNT; i++){
                rain.getSprite(i).draw(app.batch);
            }
        }
        app.font48.draw(app.batch,score,app.camera.position.x+70-textOffset,RainyBee.WORLD_HEIGHT-10);
        app.font48.draw(app.batch,time,app.camera.position.x+70,RainyBee.WORLD_HEIGHT-10);
		app.batch.end();
    }

    private void update(float dt) {
        handleInput();
        //set state to dead or won
        if(LIFE<=0){
            gameState = GameState.DEAD;
        }else if(TIME<=0){
            gameState = GameState.WON;
        }

        background.update(dt,app.camera.position.x, app.camera.viewportWidth);
        bee.update(dt);

        //normal game state
        if(gameState.equals(GameState.NORMAL)){
            rain.update(dt,app.camera.position.x, app.camera.viewportWidth, bee.getBounds());//see comments above
            pollen.update(dt,app.camera.position.x, app.camera.viewportWidth, bee.getBounds());
            app.camera.position.x = bee.getPosition().x+300;
            app.camera.update();

            //handling time
            timeSeconds += Gdx.graphics.getRawDeltaTime();
            if(timeSeconds>period){
                timeSeconds-=period;
                TIME--;
            }
        }

        score = "Pollen : "+ POLLEN;
        time = "Time : "+TIME;

    }

    /*we only handle one event*/
    private void handleInput() {
        if(Gdx.input.justTouched()){
            if(gameState.equals(GameState.NORMAL)){
                bee.jump();
            }else{
                app.setScreen(app.endScreen);
            }
        }
    }

    public void setDifficulty(){
        int dif = 300; // app.preferences.getInteger("difficulty");
        rain.setDifficulty(dif);
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width,height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
       gameover.dispose();
    }
}
