package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kryptontechx.bee.RainyBee;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
/**
 * Created by dulip on 9/5/17.
 */

public class MainMenue implements Screen {
    private RainyBee app;
    private Stage stage;
    private Skin skin;
    private Sound click;
    private ImageButton btnExit,btnHS,btnUnlock;
    private ImageButton btnPlay, btnInfo,btnSettings;
    private Image bg;
    private TextureAtlas atlas;

    public MainMenue(RainyBee app){
        this.app = app;
        stage = new Stage(new StretchViewport(RainyBee.MWIDTH,RainyBee.MHEIGHT,app.camera),app.batch);
        skin = new Skin();
    }

    @Override
    public void show() {
        stage.clear();
        app.camera.position.x = RainyBee.MWIDTH/2; // since our camera moves in the playscreen we have to reset it here, as we are not creating new menue
        app.camera.position.y = RainyBee.MHEIGHT/2;

        skin.addRegions(app.assets.get("ui/orange/uiskin.atlas", TextureAtlas.class));
        skin.add("default-font",app.font36); //changed the orange/uiskin.json to this
        skin.load(Gdx.files.internal("ui/orange/uiskin.json"));
        Gdx.input.setInputProcessor(stage);
        initButtons();

        click = app.assets.get("sound/button.ogg", Sound.class);
        click.setVolume(1,2.0f);
    }

    private void initButtons() {

        bg = new Image(app.assets.get("png/menue2.png",Texture.class));
        bg.setPosition(0,0);

        atlas = app.assets.get("ui/menue_ui.atlas", TextureAtlas.class);
        SpriteDrawable play = new SpriteDrawable(new Sprite(app.assets.get("ui/play.png", Texture.class)));
        //SpriteDrawable play = new SpriteDrawable(new Sprite(atlas.findRegion("play")));
        play.getSprite().setSize(240,240);
        btnPlay = new ImageButton(skin,"play");
        btnPlay.setSize(240,240);
        btnPlay.getStyle().imageUp = play;
        btnPlay.getStyle().imageDown = play;

        //twice the size of smaller button takes space of 2 small buttons
        btnPlay.setPosition(RainyBee.MWIDTH/2-120, 320); // world height is 630
        btnPlay.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.playScreen);
            }
        });
        btnPlay.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));
////////////////////////////////////////////////////////play button over//////////////////////////////////////////////////////

        SpriteDrawable settings = new SpriteDrawable(new Sprite(atlas.findRegion("setting")));
        SpriteDrawable info = new SpriteDrawable(new Sprite(atlas.findRegion("home")));
        SpriteDrawable close = new SpriteDrawable(new Sprite(atlas.findRegion("close3icon")));
        SpriteDrawable hs = new SpriteDrawable(new Sprite(atlas.findRegion("trophy")));
        SpriteDrawable unlock = new SpriteDrawable(new Sprite(atlas.findRegion("lock")));

        btnInfo = new ImageButton( skin,"info");
        btnInfo.getStyle().imageUp = info;
        btnInfo.getStyle().imageDown = info;

        btnSettings =  new ImageButton( skin, "settings");
        btnSettings.getStyle().imageDown = settings;
        btnSettings.getStyle().imageUp = settings;

        btnExit =  new ImageButton(skin, "exit");
        btnExit.getStyle().imageDown = close;
        btnExit.getStyle().imageUp = close;

        btnHS = new ImageButton(skin,"highscore");
        btnHS.getStyle().imageUp = hs;
        btnHS.getStyle().imageDown=hs;

        btnUnlock = new ImageButton(skin,"unlocks");
        btnUnlock.getStyle().imageDown=unlock;
        btnUnlock.getStyle().imageUp=unlock;

/////////////////////
        int size = 100;
        int padDown = RainyBee.MWIDTH/2-size/2;
        int padUp = RainyBee.MWIDTH/2+size/2;
        //verticalPos_buttons4 the button position for the 4 buttons in the bottom of two main buttons
        float vPos = 90;

        btnHS.setSize(size,size);
        btnHS.setPosition(padDown-300, vPos-4); //slightly offset vertical position as there is annoying error in texturepacker
        btnHS.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                //app.setScreen(app.highScore);
            }
        });
        btnHS.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));

        ////////
        btnUnlock.setSize(size,size);
        btnUnlock.setPosition(padDown-150, vPos); //right edge is 315-140px
        btnUnlock.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.unlocks);
            }
        });
        btnUnlock.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));
        ///////
        btnInfo.setSize(size,size);
        btnInfo.setPosition(padDown, vPos);
        btnInfo.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.credits);

            }
        });
        btnInfo.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));
        ///////
        btnSettings.setSize(size,size);
        btnSettings.setPosition(padUp+50, vPos);
        btnSettings.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                app.setScreen(app.settings);
            }
        });
        btnSettings.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));
        ////////
        btnExit.setSize(size,size);
        btnExit.setPosition(padUp+200, vPos);
        btnExit.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                click.play();
                Gdx.app.exit();
            }
        });
        btnExit.addAction(sequence(
                alpha(0),
                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
        ));
        ///////
        stage.addActor(bg);
        stage.addActor(btnPlay);
        stage.addActor(btnInfo);
        stage.addActor(btnSettings);
        stage.addActor(btnExit);
        stage.addActor(btnHS);
        stage.addActor(btnUnlock);
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(1.0f,1.0f, 1.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    private void update(float delta) {
        stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        skin.dispose();
    }
}
//public class MainMenue implements Screen {
//    private RainyBee app;
//    private Stage stage;
//    private Skin skin;
//    private Sound click;
//    private TextButton btnPlay, btnExit, btnCredits, btnSettings;
//    //private BitmapFont font;
//
//    public MainMenue(RainyBee app){
//        this.app = app;
//        stage = new Stage(new StretchViewport(RainyBee.WORLD_WIDTH/2,RainyBee.WORLD_HEIGHT,app.camera),app.batch);
//        skin = new Skin();
//    }
//
//    @Override
//    public void show() {
//        stage.clear();
//        app.camera.position.x = RainyBee.WORLD_WIDTH/4; // since our camera moves in the playscreen we have to reset it here, as we are not creating new menue
//        //app.music.setVolume(0.6f);
//        skin.addRegions(app.assets.get("ui/orange/uiskin.atlas", TextureAtlas.class));
//        skin.add("default-font",app.font36); //changed the orange/uiskin.json to this
//        skin.load(Gdx.files.internal("ui/orange/uiskin.json"));
//        Gdx.input.setInputProcessor(stage);
//        initButtons();
//
//        click = app.assets.get("sound/button.ogg", Sound.class);
//        click.setVolume(1,2.0f);
//    }
//
//    @Override
//    public void render(float delta) {
//        update(delta);
//        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1);
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//        stage.draw();
//    }
//
//    private void update(float delta) {
//        stage.act(delta);
//    }
//
//    @Override
//    public void resize(int width, int height) {
//        stage.getViewport().update(width,height,false);
//    }
//
//    @Override
//    public void pause() {
//
//    }
//
//    @Override
//    public void resume() {
//
//    }
//
//    @Override
//    public void hide() {
//
//    }
//
//    private void initButtons() {
//        //btnHC means button height control
//        float btnHC = RainyBee.WORLD_HEIGHT - 550;  // means we start at 80 in libgdx draw(from bot left)
//
//        btnPlay = new TextButton("Start",skin,"default");
//        btnCredits = new TextButton("About", skin,"default");
//        btnSettings =  new TextButton("Settings", skin, "default");
//        btnExit =  new TextButton("Exit", skin, "default");
//
//        btnPlay.setSize(120,80);
//        btnPlay.setPosition(RainyBee.WORLD_WIDTH/4-60,btnHC+420); // world height is 630
//        btnPlay.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                click.play();
//                app.setScreen(app.playScreen);
//            }
//        });
//        btnPlay.addAction(sequence(
//                alpha(0),
//                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
//        ));
//
//        btnCredits.setSize(120,80);
//        btnCredits.setPosition(RainyBee.WORLD_WIDTH/4-60,btnHC+280);//difference between height is 140
//        btnCredits.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                click.play();
//                app.setScreen(app.credits);
//
//            }
//        });
//        btnCredits.addAction(sequence(
//                alpha(0),
//                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
//        ));
//
//        btnSettings.setSize(120,80);
//        btnSettings.setPosition(RainyBee.WORLD_WIDTH/4-60,btnHC+140);
//        btnSettings.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                click.play();
//                app.setScreen(app.settings);
//            }
//        });
//        btnSettings.addAction(sequence(
//                alpha(0),
//                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
//        ));
//
//        btnExit.setSize(120,80);
//        btnExit.setPosition(RainyBee.WORLD_WIDTH/4-60,btnHC);
//        btnExit.addListener(new ClickListener(){
//            @Override
//            public void clicked(InputEvent event, float x, float y) {
//                click.play();
//                Gdx.app.exit();
//            }
//        });
//        btnExit.addAction(sequence(
//                alpha(0),
//                parallel(fadeIn(0.5f),moveBy(0,-20,0.5f))
//        ));
//
//        stage.addActor(btnPlay);
//        stage.addActor(btnCredits);
//        stage.addActor(btnSettings);
//        stage.addActor(btnExit);
//    }
//
//    @Override
//    public void dispose() {
//        stage.dispose();
//        skin.dispose();
//    }
//}