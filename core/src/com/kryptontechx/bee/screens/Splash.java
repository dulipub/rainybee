package com.kryptontechx.bee.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kryptontechx.bee.RainyBee;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

/**
 * Created by dulip on 9/3/17.
 * Viewport: the viewport we need to set for the play state is the extend viewport but for splash we
 * settled for stretched viewport
 */
public class Splash implements Screen {
    private RainyBee app;
    private Stage stage;
    private Image splashImg;

    public Splash(RainyBee app){
        this.app = app;
        stage = new Stage(new StretchViewport(RainyBee.WORLD_WIDTH/2,RainyBee.WORLD_HEIGHT,app.camera));

    }
    @Override
    public void show() {
        stage.clear();
        Gdx.input.setInputProcessor(stage);
        Texture splashTex = new Texture(Gdx.files.internal("badlogic.jpg"));
        splashImg = new Image(splashTex);
        splashImg.setSize(200,200);
        //final position of the image is in the center, (width-imgWidth)/2,(height-imgHeight)/2
        //but we set the final Y position by action in moving 100 pixels down.
        splashImg.setPosition(stage.getWidth()/2-100,stage.getHeight()/2);

        Runnable transitToMenue = new Runnable() {
            @Override
            public void run() {
                app.setScreen(app.mainMenue);
            }
        };

        splashImg.addAction(sequence(
                alpha(0.0f),
                parallel(fadeIn(2),moveBy(0,-100,2)),
                delay(1.5f),
                fadeOut(1.25f),
                run(transitToMenue)
        ));

        stage.addActor(splashImg);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        update(delta); // calls the "update" method in stage
        stage.draw(); // "render" method of stage
    }

    private void update(float delta) {
        stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
