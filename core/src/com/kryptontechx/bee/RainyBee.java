package com.kryptontechx.bee;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.kryptontechx.bee.screens.*;

/**
 * Removes credis and settings screens to keep the app ligh as possible when running
 *
 * */

public class RainyBee extends Game {

	public static int WORLD_WIDTH = 1260;
	public static int WORLD_HEIGHT = 630;
	public static int MWIDTH = 900;
	public static int MHEIGHT = 600;
	public SpriteBatch batch;
	public OrthographicCamera camera;
	public AssetManager assets;
	public BitmapFont font48,font36,font24,font24G;
	public Music music;
	public Preferences preferences;

	public Loading loading;
	public PlayScreen playScreen;
	public MainMenue mainMenue;
	public EndScreen endScreen;
	public Credits credits;
	public Settings settings;
	public Unlocks unlocks;
	
	@Override
	public void create () {
		assets = new AssetManager();
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false,WORLD_WIDTH/2,WORLD_HEIGHT);

		initFonts();

		loading = new Loading(this);
		playScreen = new PlayScreen(this);
		mainMenue = new MainMenue(this);
		endScreen = new EndScreen(this);
		credits = new Credits(this);
		settings = new Settings(this);
		unlocks = new Unlocks(this);

		setScreen(loading);

		preferences = Gdx.app.getPreferences("Rainybee");
		initPrefs(); //checks preferances

		music = Gdx.audio.newMusic(Gdx.files.internal("sound/music.ogg"));
		music.setVolume(0.3f);
		music.setLooping(true);
		music.play();
	}

	private void initPrefs() {
		if(!preferences.contains("first")){
			preferences.putBoolean("first",true);
			preferences.putInteger("music",50);
			preferences.putInteger("lives",2);
			//this is the bee that will be displayed when played
			preferences.putInteger("beeType",0);
			//won/attempts count
			preferences.putInteger("attempts",0);
			preferences.putInteger("won",0);
			//difficulty means the max speed of the (to get velocity we must multiply by -1 in play state)
			preferences.putInteger("difficulty",350);
		}else{
			preferences.putBoolean("first",false);
		}
//		int a = preferences.getInteger("lives");
//		System.out.println(a);
		preferences.flush();
	}

	private void initFonts() {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/sujet.ttf"));
		//FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/infini-gras.otf"));
		FreeTypeFontGenerator.FreeTypeFontParameter params = new FreeTypeFontGenerator.FreeTypeFontParameter();

		params.size = 36;
		params.color = Color.GOLD;
		//params.borderWidth = 0.7f;
		font36 = generator.generateFont(params);
		font36.setUseIntegerPositions(false);

		params.size = 26;
		params.color = Color.BLACK;
		font24 = generator.generateFont(params);
		font24.setUseIntegerPositions(false);

		params.size = 40;
		params.color = Color.GOLD;
		params.borderColor = Color.GOLD;
		font48 = generator.generateFont(params);
		font48.setUseIntegerPositions(false);

		params.size = 22;
		font24G = generator.generateFont(params);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {

		batch.dispose();
		assets.dispose();;
		loading.dispose();
		playScreen.dispose();
		mainMenue.dispose();
		endScreen.dispose();
		credits.dispose();
		settings.dispose();
		music.dispose();
	}
}
